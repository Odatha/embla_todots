//import { observable } from "mobx";

export class TodoStore {
  lists = [
    {
      edit: false,
      key: 1,
      text: "power on the devices",
      date: "5-6-2019"
    },
    {
      edit: false,
      key: 2,
      text: "dinner party",
      date: "6-6-2019"
    },
    {
      edit: false,
      key: 3,
      text: "power on the devices",
      date: "10-6-2019"
    }
  ];

  getTodoList() {
    return this.lists;
  }
}
