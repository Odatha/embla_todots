import React, { Component } from "react";
import TodoLists from "./todo-lists";

import FormTodo from "./form-todo";
import FormInput from "./form-input";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import { TodoStore } from "../stores/todo-store";
//import { any } from "prop-types";
//import {TodoStore} from "./todoStore";

@observer
class Todo extends Component {
  @observable edit: boolean = false;
  @observable key: number = Date.now();
  @observable text: string = " ";
  @observable date: string = " ";
  @observable lists: {
    edit?: boolean;
    key: number;
    text: string;
    date: string;
  }[] = [];

  @action("setEditStatus Todo")
  setEditStatus(status: boolean) {
    this.edit = status;
  }

  constructor(props) {
    super(props);
    //this.state = new TodoStore();
    const listsNew = new TodoStore();
    const listData = listsNew.getTodoList();
    this.lists = listData;

    this.addList = this.addList.bind(this);
  }

  @action
  deleteList(deleteItem) {
    var filterItems = this.lists.filter(newLists => {
      return newLists !== deleteItem;
    });
    this.lists = filterItems;
    // const todo = new TodoStore();
    // todo.getTodoList();
  }

  @action
  onEditHandle(e) {
    // this.lists({
    //   edit: true[0],
    //   key: arguments[1],
    //   text: arguments[2],
    //   date: arguments[3]
    // });
    var x;
    this.lists[x].edit = true;
  }

  @action
  onUpdateHandle(e) {
    e.preventDefault();
    // this.lists({
    //   lists: this.lists.map(item => {
    //     if (item.key === this.key) {
    //       item["text"] = e.target.updatedItem.value;
    //       item["date"] = e.target.updatedDate.value;
    //       return item;
    //     }
    //     return item;
    //   })
    // });
    var x;
    this.lists = this.lists.map(item => {
      if (item.key === this.lists[x].key) {
        //item.edit = false;
        item.text = e.target.updatedItem.value;
        item.date = e.target.updatedDate.value;

        return item;
      }
      return item;
    });

    this.lists[x].edit = false;
  }

  @action addList(e) {
    e.preventDefault();
    //this.setEditStatus(true);

    // if (this.edit) {
    // }

    this.lists = [
      ...this.lists,
      {
        //edit: e.taregt.item.value,
        key: e.target.item.value,
        text: e.target.item.value,
        date: e.target.pickedDate.value
      }
    ];

    e.target.item.value = "";
    //console.log(e.target.item.key);
  }

  renderEditForm() {
    let x;
    if (this.lists[x]) {
      return (
        <FormTodo onSubmit={this.onUpdateHandle.bind(this)} label="Update">
          <FormInput
            name={"updatedItem"}
            type={"text"}
            defaultValue={this.lists[x].text}
          ></FormInput>
          <FormInput
            name={"updatedDate"}
            type={"date"}
            defaultValue={this.lists[x].date}
          ></FormInput>
        </FormTodo>
      );
    }
  }

  render() {
    return (
      <div className="mainList">
        <div className="header">
          {this.renderEditForm()}
          <FormTodo onSubmit={this.addList.bind(this)} label="Add">
            <FormInput
              type={"text"}
              name={"item"}
              placeholder={"what are your todo s??"}
            ></FormInput>

            <FormInput type={"date"} name={"pickedDate"}></FormInput>
          </FormTodo>
          <TodoLists
            onEditHandle={this.onEditHandle.bind(this)}
            deleteList={this.deleteList.bind(this)}
            displays={this.lists}
          />
        </div>
      </div>
    );
  }
}
export default Todo;
